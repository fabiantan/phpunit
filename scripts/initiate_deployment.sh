#!/bin/bash
ROOTDIRECTORY9TRO="/var/www/9tro"
CURRENTTIME=`date +%Y%m%d%H%M%S`
DIRECTORY9TRO=$ROOTDIRECTORY9TRO-$CURRENTTIME


if [ "$DEPLOYMENT_GROUP_NAME" == "9troDevelopment" ]
then
	if [ -d /tmp/deployment/9tro ]; then
		mkdir -p /var/www
 		mv /tmp/deployment/9tro $DIRECTORY9TRO
	else
		echo "Directory does not exist"; exit 1
	fi

	if [ $? == 0 ]; then 

		if [ -L "$ROOTDIRECTORY9TRO" ]; then rm -f $ROOTDIRECTORY9TRO; 
		elif [ -d "$ROOTDIRECTORY9TRO" ]; then mv $DIRECTORY9TRO $DIRECTORY9TRO.bak; 
		fi

		ln -s $DIRECTORY9TRO $ROOTDIRECTORY9TRO; 


	else
	   	exit 1
	fi
fi
